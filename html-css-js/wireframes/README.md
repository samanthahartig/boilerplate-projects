# **Wireframing Your App**

When we start to design our applications, it's helpful to have an idea of what the screens will look like. You could invest weeks on perfecting pixel-perfect designs, or you could spend a couple of hours creating a low-fidelity representation of what the application *should* look like, without the colors, pictures, and the "unimportant" text.

For example, here's a screenshot of a Web application, Twitter:

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/48b1d9d085fba213e74878d7d2f6d7a5.png

Here's what a wireframe for that page could look like (click on the image to see a larger version):

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/dcb691eb3cd5d386dd633b5950e1d8eb.png

Notice that only the main elements and layout have any definition to them. The rest are just indications.

- The navigation on the left side has the important menu items actually specified
- The layout of the page with the main Tweet button has some definition to it
- We put "placeholder text" lines to indicate where text should go
- We put explanations around the wireframe (and use arrows when we need to) to explain what's being shown on the wireframe

# **How to wireframe**

Use wireframing as a way to have conversations with your team about the human interactions that will occur with your application. This is about **capturing interactions**, not doing high-fidelity designs.

**Collaboration is key!** Everyone should talk through the designs so that everyone has an idea of the way that the application will work. Everyone should contribute to the ideas in the design, as well as checking consistency of the application's design.

# **Wireframing in Excalidraw**

There are a lot of tools available on the Internet for you to do this wireframing. However, they either cost money, don't allow you to work as a team, or are very complex.

Excalidraw, on the other hand, has an easy, nice, and free way to make wireframes.

To get ready to wireframe, everybody on the team that wants to participate in drawing should import the [Lo-Fi Wireframing Kit](https://libraries.excalidraw.com/?target=_excalidraw&theme=light&sort=default#spfr-lo-fi-wireframing-kit) into their library.

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/58daafe917dc8632809a892d4ee2af4f.png

This will import 23 shapes into Excalidraw that you can use in your wireframes by clicking the Library button at the top of the Excalidraw screen to the right of the toolbar.

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/fc29adcfa2178bd791ae938b0609bb84.png

Then, you can drag those components onto your Excalidraw drawing surface. There's a video at the end of this page to show you how that works.

# **PRO TIP: Keep the sidebar open**

Click the "Keep the sidebar open" button so that you don't have to keep opening it over and over when using shapes from the library.

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/3cee91562be254a00c177818e9291fb9.png

# **Export your drawing**

When you finish with your wireframes, you'll want to save them so that you don't lose them. Make sure to export them by clicking the "Export" button in the top-left menu item.

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/92531e2ac59c80ee4492a1f40eebc02a.png

Then, choose "Save to disk" so that you have them for later.

!https://s3.us-west-2.amazonaws.com/forge-production.galvanize.com/content/1c9c7db79abce3c55d73d91d0c3632ab.png

A great place to put these is in your GitLab project repository! If you already have one, create a *wireframes* directory and put any exported Excalidraw downloads in there, then add, commit, and push them.

# **Presenting your wireframes**

Your team will present your wireframes so that you can share your excitement with the class, as well as get feedback.

Make sure that you save that Excalidraw file so that you can open it, again, when you need to.

# **Watch a video**

If you're interested in seeing how to wireframe, here's a short video that walks you through the process.

# Endpoint Template

Use the following template to describe each of your endpoints.

It's not uncommon for a non-trivial application to have at least 30 endpoints. Don't be afraid of "too many."

Everything between the guillemots (`«` and `»`) should be replaced by you to describe the endpoint.

```markdown
### «Human-readable of the endpoint»* Endpoint path: «path to use»
* Endpoint method: «HTTP method»
* Query parameters:
  * «name»: «purpose»

* Headers:
  * Authorization: Bearer token

* Request shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```
* Response: «Human-readable description
            of response»
* Response shape (JSON):
    ```json
    «JSON-looking thing that has the
    keys and types in it»
    ```

```

Mandatory fields are:

- `Endpoint path`
- `Endpoint method`
- `Response`
- `Response shape`

If your endpoint needs to know who the person is, then include the `Headers/Authorization` part.

If your endpoint is a POST, PUT, or PATCH, include the `Request shape (JSON)` part.
