# WebSockets In Django

Django, by itself, does not have any support for WebSockets. However, there's a library named [Django Channels](https://channels.readthedocs.io/)  that you can use to enable that support. It's a little more difficult than FastAPI, but still totally doable.

We're just testing this out to see how it works. When we want to integrate our stuff into our project, we'll be more careful about everything. At the end, we'll talk about how to integrate this into an existing Django project.

# **Getting set up**

Create a directory for this playful project to live in. **Do not** put this in an existing project. Create its own directory somewhere.

It may have been a while since you last created a virtual environment, so here are those steps, again:

1. Create the virtual environment

    ```bash
    python -m venv .venv

    ```

2. Activate the virtual environment the way you do on your operating system
3. Upgrade **pip** in your virtual environment

    ```bash
    python -m pip install --upgrade pip

    ```

4. Install the dependencies with the following command

    ```bash
    pip install django channels

    ```

5. Create a Django project

    ```bash
    django-admin startproject chat_project .

    ```

6. Create a Django app

    ```bash
    python manage.py startapp chat_app

    ```


# **Configuring the Django project**

There are some normal things that we need to do to configure the Django project and the Django app.

1. Install the Django app `chat_app.apps.ChatAppConfig` in the `INSTALLED_APPS` of the project
2. Install the `channels` library in the `INSTALLED_APPS` of the project

Now, there's Django Channels stuff that we need to add so we can use WebSockets.

### **Specify the async application**

In the *settings.py*, add this new value that's used by the Django Channels library to figure out what the correct module is to run when the application starts:

```python
ASGI_APPLICATION = "chat_project.asgi.application"
```

### **Create a channel layer**

The Django Channels library needs a way to collect all of the different WebSockets so that you can broadcast a message to all of the connected browsers or other clients. To do that, add this to the *settings.py*, as well:

```python
CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels.layers.InMemoryChannelLayer"
    }
}

```

The `InMemoryChannelLayer` is **only** for testing. If you want to use Django Channels in production, you should use a different Channel Layer. Please see [the documentation](https://channels.readthedocs.io/en/stable/topics/channel_layers.html)  .

### **Update the async entry point**

In the `chat_project`, update the *asgi.py* file to have this content:

```python
import os
from channels.routing import ProtocolTypeRouter
from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'chat_project.settings')

# Let the ProtocolTypeRouter figure out if this is a# normal HTTP request that should have HTML sent back,# or a WebSocket request that should be handled by the# Django Channels stuff
application = ProtocolTypeRouter({
    "http": get_asgi_application(),
})

```

# **Create a view and a template**

In the `chat_app`, create a function view that just renders a template named *index.html*. Here's the easiest way to do that:

```python
from django.shortcuts import render

def chat_index(request):return render(request, "chat/index.html")

```

Now, create that *index.html* file as a template in a *chat* subdirectory so that it's found.

In the *index.html* file, put this HTML, which is nearly identical to what we did for the FastAPI HTML. The only difference is that the incoming message from the WebSocket is a JSON string, so we need to parse it.

```html
<!DOCTYPE html><html><head><title>Chat</title></head><body><h1>WebSocket Chat</h1><form id="message-form"><input type="text" id="messageText" autocomplete="off"/><button>Send</button></form><ul id='messages'></ul><script>
      // Create a new WebSocket to the FastAPI serverconst ws = new WebSocket(`ws://${location.host}/ws/`);

      // When a message arrives, handle it with this function
      ws.addEventListener('message', event => {
        const data = JSON.parse(event.data);
        const messages = document.getElementById('messages')
        const message = document.createElement('li')
        const content = document.createTextNode(data.message)
        message.appendChild(content)
        messages.appendChild(message)
      });

      // Get the form and, when someone submits it, cancel// that event and send the message over the WebSocketconst form = document.getElementById('message-form');
      form.addEventListener('submit', event => {
        event.preventDefault()
        const input = document.getElementById("messageText")
        ws.send(input.value)
        input.value = ''
      });
    </script></body></html>
```

Check out the comments in the JavaScript about what the code does.

Now, do what you'd normally do to make that function view active. Create a *urls.py* file in the `chat_app` Django app and put this code in there:

```python
from django.urls import path
from .views import chat_index

urlpatterns = [
    path("", chat_index, name="chat_index"),
]

```

Include the Django app's paths from *chat_app/urls.py* in the project's *urls.py*.

```python
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path("", include("chat_app.urls")),
    path('admin/', admin.site.urls),
]

```

# **Start the application**

You can use the same old command to run the Django development Web server:

```bash
python manage.py runserver

```

If you read the output, you'll see something new in there:

```
Starting ASGI/Channels version 3.0.5 development
server at http://127.0.0.1:8000/

```

This is different from the normal way that the Django development Web server runs. The Django Channels has taken over the processing of all incoming connections because you changed the *asgi.py* file and added the `ASGI_APPLICATION` setting.

Open your browser to [http://localhost:8000](http://localhost:8000/)  and open your browser development tools. Refresh the page. You should see an error message in the Console that reports the WebSocket connection failed. This happens because we haven't written any code to actually handle the WebSocket connection, yet.

You'll also see in your Django development Web server output some messages like these:

```
ValueError: No application configured for scope type 'websocket'
WebSocket DISCONNECT /ws [127.0.0.1:51717]

```

We have yet to configure the WebSocket portion of the Django Channels library, so it tells us there's nothing configured to handle `'websocket'` connections.

To handle WebSocket connections, we're going to write a class-based handler that's like a class-based view, but handles WebSocket connections rather than HTTP requests.

# **Create and register a consumer**

Instead of being called "views" and "urls", the Django Channels library uses the terms "consumers" and "routes".

- **Consumers** handle the incoming WebSocket connection, like a view handles incoming HTTP requests
- **Routes** determine which consumer is used for a WebSocket connection, like a `path` entry in the *urls.py* figures out what view is used for incoming HTTP requests

### **Create the consumer**

In your `chat_app` Django app, create a new file named *consumers.py*. This file is like *views.py*, but for WebSockets. In that file, add this code:

```python
import json
from channels.generic.websocket import AsyncWebsocketConsumer

class ChatConsumer(AsyncWebsocketConsumer):async def connect(self):
        self.room_name = "default"
        self.room_group_name = 'chat_%s' % self.room_name

        # Join the default roomawait self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):# Leave the default roomawait self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocketasync def receive(self, text_data):# Send a message to everyone in the# default roomawait self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': text_data
            }
        )

    # Receive message from room groupasync def chat_message(self, event):
        message = event['message']

        # Send message to WebSocketawait self.send(text_data=json.dumps({
            'message': message
        }))

```

Notice that we're using `async` methods and `await` keywords in this code. This is the first time that we've done that with Django. We can do this because Django Channels is using the *asgi.py* file rather than the *wsgi.py* file to run Django for us.

### **Database access is different**

If you want to access the database through your models, you'll need to do some special coding for that. Please see [Database Access](https://channels.readthedocs.io/en/stable/topics/databases.html)  in the Django Channels documentation to discover how to use the `database_sync_to_async` function to do that.

### **Create the route**

In your `chat_app` Django app, create a new file named *routing.py*. This file is like *urls.py*, but for WebSockets. In *routing.py*, add a route for the `ChatConsumer` from the last section:

```python
from django.urls import path

from .consumers import ChatConsumer

websocket_urlpatterns = [
    path("ws/", ChatConsumer.as_asgi()),
]

```

We use the `path` function to register the path, just like we would for a normal view.

Instead of using the `urlpatterns` variable, we use the `websocket_urlpatterns` as a **convention**. It does not need to be named that.

The `.as_asgi()` method on the `ChatConsumer` is like the `.as_view()` method on class-based views. It converts the `ChatConsumer` to a function to be called when a WebSocket connection is made to the path `/ws/`.

### **Register the WebSocket protocol**

Back in *asgi.py* in the Django project, update it to have this code, which adds the `websocket` protocol:

```python
import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "chat_project.settings")
django_asgi_app = get_asgi_application()

import chat_app.routing

# Let the ProtocolTypeRouter figure out if this is a# normal HTTP request that should have HTML sent back,# or a WebSocket request that should be handled by the# Django Channels stuff
application = ProtocolTypeRouter({
    "http": django_asgi_app,
    "websocket": AllowedHostsOriginValidator(
        # Use authentication for protected WebSocket# connections
        AuthMiddlewareStack(
            # Route the WebSocket connection based on the# URL path
            URLRouter(
                chat_app.routing.websocket_urlpatterns,
            )
        )
    ),
})

```

# **Try it out**

Make sure the application is still running by looking at your terminal to make sure there are no errors.

You'll need to refresh your browser(s) to reestablish the WebSocket connection.

Now, you can send messages to all of the connected browsers!

# **Using in your project**

Django Channels is meant to be as unobtrusive as possible. You should be able to update the *asgi.py* file in your Django project, and everything should Still Just Work.

The **main difference** is how you run your application. Because you've been using the WSGI version of Django, you have been using Gunicorn to run the application in your Dockerfiles. That needs to change to an ASGI application runner, the same way that FastAPI runs.

The Django Channels project maintains its own ASGI server named [Daphne](https://github.com/django/daphne)  . To use it, just add it to your *requirements.txt* file the same way you would any other dependency by finding its version number on [PyPi](https://pypi.org/project/daphne/)  . Then, run your application with the following command, replacing the `gunicorn` part with this and modifying the `myproject` part to be the name of your Django project:

```bash
daphne -b 0.0.0.0 -p $PORT myproject.asgi:application

```

You can read more about deploying [here](https://channels.readthedocs.io/en/stable/deploying.html)  .
